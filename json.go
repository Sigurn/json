package json

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"reflect"
)

var (
	FormatError    = errors.New("Invalid JSON format")
	DelimiterError = errors.New("Invalid JSON delimiter")
)

type KeyNotFoundError struct {
	Key string
}

type TypeConversionError struct {
	SourceType reflect.Type
	DestType   reflect.Type
}

func (err KeyNotFoundError) Error() string {
	return "Key not found"
}

func (err TypeConversionError) Error() string {
	return "Cannot perform type conversion"
}

func IsObject(value interface{}) bool {
	_, ok := value.(*Object)
	return ok
}

func IsArray(value interface{}) bool {
	_, ok := value.(*Array)
	return ok
}

func IsBool(value interface{}) bool {
	_, ok := value.(bool)
	return ok
}

func IsInt64(value interface{}) bool {
	_, err := AsInt64(value)
	return err == nil
}

func IsFloat64(value interface{}) bool {
	_, err := AsFloat64(value)
	return err == nil
}

func IsString(value interface{}) bool {
	_, ok := value.(string)
	return ok
}

func AsObject(value interface{}) (*Object, error) {
	if obj, ok := value.(*Object); ok {
		return obj, nil
	}
	return &Object{}, TypeConversionError{reflect.TypeOf(value), reflect.TypeOf((*Object)(nil)).Elem()}
}

func AsArray(value interface{}) (*Array, error) {
	if array, ok := value.(*Array); ok {
		return array, nil
	}
	return &Array{}, TypeConversionError{reflect.TypeOf(value), reflect.TypeOf((*Array)(nil)).Elem()}
}

func AsBool(value interface{}) (bool, error) {
	if flag, ok := value.(bool); ok {
		return flag, nil
	}
	return false, TypeConversionError{reflect.TypeOf(value), reflect.TypeOf((*bool)(nil)).Elem()}
}

func AsString(value interface{}) (string, error) {
	if str, ok := value.(string); ok {
		return str, nil
	}
	return "", TypeConversionError{reflect.TypeOf(value), reflect.TypeOf((*string)(nil)).Elem()}
}

func AsNumber(value interface{}) (json.Number, error) {
	switch value.(type) {
	case json.Number:
		return value.(json.Number), nil
	case int64, float64:
		return json.Number(fmt.Sprintf("%v", value)), nil
	}
	return "", TypeConversionError{reflect.TypeOf(value), reflect.TypeOf((*json.Number)(nil)).Elem()}
}

func AsInt64(value interface{}) (int64, error) {
	switch value.(type) {
	case json.Number:
		return value.(json.Number).Int64()
	case int64:
		return value.(int64), nil
	case float64:
		return int64(value.(float64)), nil
	}
	return 0, TypeConversionError{reflect.TypeOf(value), reflect.TypeOf((*int64)(nil)).Elem()}
}

func AsFloat64(value interface{}) (float64, error) {
	switch value.(type) {
	case json.Number:
		return value.(json.Number).Float64()
	case float64:
		return value.(float64), nil
	case int64:
		return float64(value.(int64)), nil
	}
	return 0, TypeConversionError{reflect.TypeOf(value), reflect.TypeOf((*float64)(nil)).Elem()}
}

func parseArray(decoder *json.Decoder) (*Array, error) {
	array := &Array{make([]interface{}, 0)}
	for {
		token, err := decoder.Token()
		if err != nil {
			return &Array{nil}, err
		}
		if delim, ok := token.(json.Delim); ok {
			switch delim.String() {
			case "[":
				arr, err := parseArray(decoder)
				if err != nil {
					return &Array{nil}, err
				}
				array.items = append(array.items, arr)
			case "]":
				return array, nil
			case "{":
				obj, err := parseObject(decoder)
				if err != nil {
					return &Array{nil}, err
				}
				array.items = append(array.items, obj)
			case "}":
				return &Array{nil}, FormatError
			default:
				return &Array{nil}, DelimiterError
			}
		} else {
			array.items = append(array.items, token)
		}
	}
}

func parseObject(decoder *json.Decoder) (*Object, error) {
	obj := &Object{make([]KeyValue, 0)}
	for {
		token, err := decoder.Token()
		if err != nil {
			return &Object{nil}, err
		}
		if delim, ok := token.(json.Delim); ok {
			if delim.String() == "}" {
				return obj, nil
			}
			return &Object{nil}, FormatError
		}
		key := fmt.Sprintf("%v", token)
		token, err = decoder.Token()
		if err != nil {
			return &Object{nil}, err
		}
		var value interface{}
		if delim, ok := token.(json.Delim); ok {
			switch delim.String() {
			case "[":
				val, err := parseArray(decoder)
				if err != nil {
					return &Object{nil}, err
				}
				value = val
			case "{":
				val, err := parseObject(decoder)
				if err != nil {
					return &Object{nil}, err
				}
				value = val
			default:
				return &Object{nil}, DelimiterError
			}
		} else if num, ok := token.(json.Number); ok {
			if i64, err := num.Int64(); err == nil {
				value = i64
			} else if f64, err := num.Float64(); err == nil {
				value = f64
			} else {
				value = token
			}
		} else {
			value = token
		}
		obj.fields = append(obj.fields, KeyValue{key, value})
	}
}

func Parse(reader io.Reader) (interface{}, error) {
	decoder := json.NewDecoder(reader)
	decoder.UseNumber()
	token, err := decoder.Token()
	if err != nil {
		return nil, err
	}
	if delim, ok := token.(json.Delim); ok {
		switch delim.String() {
		case "[":
			return parseArray(decoder)
		case "{":
			return parseObject(decoder)
		default:
			return nil, DelimiterError
		}
	}
	return nil, FormatError
}

func Dump(writer io.Writer, data interface{}) error {
	encoder := json.NewEncoder(writer)
	encoder.SetEscapeHTML(false)
	return encoder.Encode(data)
}

func DumpIndent(writer io.Writer, data interface{}, prefix string, indent string) error {
	encoder := json.NewEncoder(writer)
	encoder.SetEscapeHTML(false)
	encoder.SetIndent(prefix, indent)
	return encoder.Encode(data)
}
