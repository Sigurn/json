package json

import (
	"bytes"
	"encoding/json"
	"reflect"
	"strings"
	"testing"
)

func TestParseArray(t *testing.T) {
	const jsonStream = `
	[
		true,
		false,
		-31415927,
		3.1415927,
		"Hello world!",
		null
	]
`
	val, err := Parse(strings.NewReader(jsonStream))
	if err != nil {
		t.Errorf("Parse() failed. Error: %v", err)
	}
	if val == nil {
		t.Errorf("Parse() returned nil")
	}

	array, ok := val.(*Array)
	if !ok {
		t.Errorf("Parse() returned not an array")
	}

	if array.Len() != 6 {
		t.Errorf("array.Len() = %d != 6", array.Len())
	}

	if array.Get(0) != true {
		t.Errorf("array.Get(0)  = %v != true", array.Get(0))
	}

	if array.Get(1) != false {
		t.Errorf("array.Get(1)  = %v != false", array.Get(1))
	}

	if val, ok := array.Get(2).(json.Number); !ok || val.String() != "-31415927" {
		t.Errorf("array.Get(2)  = %v != -31415927", array.Get(2))
	}

	if val, ok := array.Get(3).(json.Number); !ok || val.String() != "3.1415927" {
		t.Errorf("array.Get(3)  = %v != 3.1415927", array.Get(3))
	}

	if array.Get(4) != "Hello world!" {
		t.Errorf("array.Get(4)  = %v != Hello world!", array.Get(4))
	}

	if array.Get(5) != nil {
		t.Errorf("array.Get(5) = %v != nil", array.Get(5))
	}
}

func TestParseObject(t *testing.T) {
	const jsonStream = `
	{
		"bool-field1": true,
		"bool-field2": false,
		"int-field": -31415927,
		"float-field": 3.1415927,
		"string-field": "Hello world!",
		"null-field": null
	}
`
	val, err := Parse(strings.NewReader(jsonStream))
	if err != nil {
		t.Errorf("Parse() failed. Error: %v", err)
	}
	if val == nil {
		t.Errorf("Parse() returned nil")
	}

	obj, ok := val.(*Object)
	if !ok {
		t.Errorf("Parse() returned not an object")
	}

	if obj.Len() != 6 {
		t.Errorf("obj.Len() = %d != 6", obj.Len())
	}

	if val, ok := obj.Get("bool-field1"); !ok || val != true {
		t.Errorf("obj.Get('bool-field1') = %v != true", val)
	}

	if val, ok := obj.Get("bool-field2"); !ok || val != false {
		t.Errorf("obj.Get('bool-filed2') = %v != false", val)
	}

	if val, ok := obj.Get("int-field"); !ok {
		t.Errorf("obj.Get('int-field')  = %v != -31415927", val)
	} else if num, ok := val.(int64); !ok || num != -31415927 {
		t.Errorf("obj.Get('int-field').(int64) = %v != -31415927", num)
	}

	if val, ok := obj.Get("float-field"); !ok {
		t.Errorf("obj.Get('float-field') = %v != 3.1415927", val)
	} else if num, ok := val.(float64); !ok || num != 3.1415927 {
		t.Errorf("obj.Get('float-field').(float64) = %v != 3.1415927", num)
	}

	if val, ok := obj.Get("string-field"); !ok || val != "Hello world!" {
		t.Errorf("obj.Get('straing-field')  = %v != Hello world!", val)
	}

	if val, ok := obj.Get("null-field"); !ok || val != nil {
		t.Errorf("obj.Get('null-field') = %v != nil", val)
	}
}

func TestDump(t *testing.T) {
	const sourceJson = `{
    "f1": true,
    "f3": -31415927,
    "f5": 3.1415927,
    "f6": "Hello world!",
    "f7": null,
    "f8": [
        "item-1",
        "item-2",
        {
            "if1": true,
            "if2": 215
        },
        true
    ],
    "f9": {
        "p1": "some string",
        "p2": true,
        "p3": -987
    }
}
`
	val, err := Parse(strings.NewReader(sourceJson))
	if err != nil {
		t.Errorf("Parse() failed. Error: %v", err)
	}
	buffer := bytes.NewBufferString("")
	err = Dump(buffer, val)
	if err != nil {
		t.Errorf("Dump() failed. Error: %v", err)
	}
	expected := `{"f1":true,"f3":-31415927,"f5":3.1415927,"f6":"Hello world!","f7":null,"f8":["item-1","item-2",{"if1":true,"if2":215},true],"f9":{"p1":"some string","p2":true,"p3":-987}}
`
	actual := buffer.String()
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Dump() = %v != %v", actual, expected)
	}

	buffer.Reset()
	err = DumpIndent(buffer, val, "", "    ")
	if err != nil {
		t.Errorf("Dump() failed. Error: %v", err)
	}
	actual = buffer.String()
	if !reflect.DeepEqual(actual, sourceJson) {
		t.Errorf("Dump() = %v != %v", actual, sourceJson)
	}
}
