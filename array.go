package json

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type Array struct {
	items []interface{}
}

func NewArray() *Array {
	return &Array{make([]interface{}, 0)}
}

func (a *Array) Len() int {
	return len(a.items)
}

func (a *Array) Get(index int) interface{} {
	return a.items[index]
}

func (a *Array) Set(index int, value interface{}) {
	a.items[index] = value
}

func (a *Array) Append(value interface{}) {
	a.items = append(a.items, value)
}

func (a *Array) Remove(index int) {
	a.items = append(a.items[:index], a.items[index+1:]...)
}

func (a *Array) Iterator() *ArrayIterator {
	return newArrayIterator(a)
}

func (a *Array) String() string {
	return fmt.Sprintf("%v", a.items)
}

func (a *Array) MarshalJSON() ([]byte, error) {
	buffer := bytes.NewBufferString("[")
	encoder := json.NewEncoder(buffer)
	encoder.SetEscapeHTML(false)
	for n, item := range a.items {
		if n != 0 {
			if _, err := buffer.WriteString(","); err != nil {
				return []byte{}, err
			}
		}
		if err := encoder.Encode(item); err != nil {
			return []byte{}, err
		}
	}
	if _, err := buffer.WriteString("]"); err != nil {
		return []byte{}, err
	}
	return buffer.Bytes(), nil
}
