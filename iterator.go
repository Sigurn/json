package json

type ArrayIterator struct {
	index int
	array *Array
}

type ObjectIterator struct {
	index int
	obj   *Object
}

func newArrayIterator(array *Array) *ArrayIterator {
	return &ArrayIterator{-1, array}
}

func newObjectIterator(obj *Object) *ObjectIterator {
	return &ObjectIterator{-1, obj}
}

func (ai *ArrayIterator) IsFirst() bool {
	return ai.index == 0 && ai.index < len(ai.array.items)
}

func (ai *ArrayIterator) IsLast() bool {
	return ai.index == (len(ai.array.items)-1) && ai.index >= 0
}

func (ai *ArrayIterator) Next() bool {
	if ai.index < len(ai.array.items)-1 {
		ai.index++
		return true
	}
	return false
}

func (ai *ArrayIterator) Current() interface{} {
	return ai.array.items[ai.index]
}

func (ai *ArrayIterator) Reset() {
	ai.index = -1
}

func (oi *ObjectIterator) IsFirst() bool {
	return oi.index == 0 && oi.index < len(oi.obj.fields)
}

func (oi *ObjectIterator) IsLast() bool {
	return oi.index == (len(oi.obj.fields)-1) && oi.index >= 0
}

func (oi *ObjectIterator) Next() bool {
	if oi.index < len(oi.obj.fields)-1 {
		oi.index++
		return true
	}
	return false
}

func (oi *ObjectIterator) Current() KeyValue {
	return oi.obj.fields[oi.index]
}

func (oi *ObjectIterator) Reset() {
	oi.index = -1
}
