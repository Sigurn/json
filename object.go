package json

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type KeyValue struct {
	Key   string
	Value interface{}
}

type Object struct {
	fields []KeyValue
}

func NewObject() *Object {
	return &Object{make([]KeyValue, 0)}
}

func (o *Object) Len() int {
	return len(o.fields)
}

func (o *Object) Get(key string) (interface{}, bool) {
	for _, kvp := range o.fields {
		if key == kvp.Key {
			return kvp.Value, true
		}
	}
	return nil, false
}

func (o *Object) GetAsObject(key string) (*Object, error) {
	if val, ok := o.Get(key); ok {
		return AsObject(val)
	}
	return &Object{}, KeyNotFoundError{key}
}

func (o *Object) GetAsArray(key string) (*Array, error) {
	if val, ok := o.Get(key); ok {
		return AsArray(val)
	}
	return &Array{}, KeyNotFoundError{key}
}

func (o *Object) GetAsBool(key string) (bool, error) {
	if val, ok := o.Get(key); ok {
		return AsBool(val)
	}
	return false, KeyNotFoundError{key}
}

func (o *Object) GetAsString(key string) (string, error) {
	if val, ok := o.Get(key); ok {
		return AsString(val)
	}
	return "", KeyNotFoundError{key}
}

func (o *Object) GetAsNumber(key string) (json.Number, error) {
	if val, ok := o.Get(key); ok {
		return AsNumber(val)
	}
	return "", KeyNotFoundError{key}
}

func (o *Object) GetAsInt64(key string) (int64, error) {
	if val, ok := o.Get(key); ok {
		return AsInt64(val)
	}
	return 0, KeyNotFoundError{key}
}

func (o *Object) GetAsFloat64(key string) (float64, error) {
	if val, ok := o.Get(key); ok {
		return AsFloat64(val)
	}
	return 0, KeyNotFoundError{key}
}

func (o *Object) Set(key string, value interface{}) {
	for n, kvp := range o.fields {
		if key == kvp.Key {
			o.fields[n] = KeyValue{kvp.Key, value}
			return
		}
	}
	o.fields = append(o.fields, KeyValue{key, value})
}

func (o *Object) Remove(key string) {
	index := -1
	for n, kvp := range o.fields {
		if key == kvp.Key {
			index = n
			break
		}
	}

	if index < 0 {
		return
	}
	o.fields = append(o.fields[:index], o.fields[index+1:]...)
}

func (o *Object) Iterator() *ObjectIterator {
	return newObjectIterator(o)
}

func (o *Object) String() string {
	return fmt.Sprintf("%v", o.fields)
}

func (o *Object) MarshalJSON() ([]byte, error) {
	buffer := bytes.NewBufferString("{")
	encoder := json.NewEncoder(buffer)
	encoder.SetEscapeHTML(false)
	for n, kvp := range o.fields {
		if n != 0 {
			if _, err := buffer.WriteString(","); err != nil {
				return []byte{}, err
			}
		}
		if _, err := fmt.Fprintf(buffer, "\"%v\":", kvp.Key); err != nil {
			return []byte{}, nil
		}
		if err := encoder.Encode(kvp.Value); err != nil {
			return []byte{}, err
		}
	}
	if _, err := buffer.WriteString("}"); err != nil {
		return []byte{}, err
	}
	return buffer.Bytes(), nil
}
